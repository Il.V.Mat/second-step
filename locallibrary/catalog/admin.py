from django.contrib import admin
#from .model import Author, Genre, Book, BookInstance
from .models import  *


# Register your models here.


admin.site.register(Book)
admin.site.register(Author)
admin.site.register(Genre)
admin.site.register(BookInstance)
