from django.contrib import admin
#from .model import Author, Genre, Book, BookInstance
from .models import  *


"""Register your models here.
 Ргистрируем здесь Модели БД"""

admin.site.register(Genre)


# добавляем и регистрируем новый класс
class AuthorAdmin(admin.ModelAdmin):        # Define a new class, - the AuthorAdmin class
    list_display = ('last_name', 'first_name', 'foto', 'date_of_birth', 'date_of_death')    # добавляет отображение перечисленных одноимённых полей из БД models.py
# Register the admin class with the associated model
admin.site.register(Author, AuthorAdmin) # а можно записать, как @admin.register(Author)
    



#Register the Admin classes for Book using the decorator
@admin.register(Book)                   # Декоратор - то же самое, что и admin.site.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'display_genre')    # добавляет отображение перечисленных полей одноимённых полей из БД models.py



# Register the Admin classes for BookInstance using the decorator
@admin.register(BookInstance)           # Декоратор - то же самое, что и admin.site.register(BookInstance)
class BookInstanceAdmin(admin.ModelAdmin):
    list_display = ('book', 'status', 'borrower', 'due_back', 'id') # добавляет отображение перечисленных полей одноимённых полей из БД models.py
    list_filter = ('status', 'due_back')            # создаст возможность фильтрации отображаемых пунктов, если в списке есть множество элементов
    fieldsets = (                               # НаборыПолей для управления макетом страниц администратора - список двух кортежей, в котором 
    #каждый из двух кортежей представляет собой <fieldset> на странице формы администратора.
        (None, {
            'fields': ('book', 'imprint', 'id') #кортеж Полей
        }),
        ('Availability', {                    #????????
            'fields': ('status', 'due_back', 'borrower')    #кортеж Полей
        }),
    )
