from django.db import models
from django.urls import reverse #Used to generate URLs by reversing the URL patterns
import uuid # Требуется для уникальных экземпляров книг - Required for unique BookInstances
from django.contrib.auth.models import User #Импорт модели User из django.contrib.auth.models
"""добавим свойство, которое можно вызвать из шаблонов, чтобы указать, просрочен ли конкретный экземпляр книги. 
Хотя можно и рассчитать это в самом шаблоне, использование свойства, - здесь будет намного более эффективным."""
from datetime import date


'''Модели Базы Данных'''
class Genre(models.Model):
    """
    Модель жанра книги (e.g. Science Fiction, Non Fiction).
    """
    name = models.CharField(max_length=200, help_text="Характеристика произведения", verbose_name='Жанр') #help_text - это заголовок каждого поля админ интерфейса
    

    def __str__(self):
        """
        String for representing the Model object (in Admin site etc.)
        Строка для представления объекта модели.
        """
        return self.name

class Book(models.Model):
    """
    Модель книги (but not a specific copy of a book).
    """
    title = models.CharField(max_length=200, verbose_name='НАЗВАНИЕ')
    author = models.ForeignKey('Author', on_delete=models.SET_NULL, null=True, verbose_name='Автор')
    # Foreign Key used because book can only have one author, but authors can have multiple books
    # Author as a string rather than object because it hasn't been declared yet in the file.
    summary = models.TextField(max_length=1000, help_text="Пару предложений о произведении", verbose_name='О чём оно?')
    isbn = models.CharField('ISBN', max_length=13,
                            help_text='13 Символов <a href="https://www.isbn-international.org/content/what-isbn">ISBN number</a>')
    genre = models.ManyToManyField(Genre, help_text="Выберите жанровую характеристику произведения", verbose_name='ЖАНР ПРОИЗВЕДЕНИЯ')

    # ManyToManyField used because genre can contain many books. Books can cover many genres.
    # Genre class has already been defined so we can specify the object above.

    def __str__(self):
        """
        String for representing the Model object.
        Строка для представления объекта модели.
        """
        return self.title

    def get_absolute_url(self):
        """
        Returns the url to access a particular book instance.
        Возвращает url-адрес для доступа к конкретному экземпляру книги.
        """
        return reverse('bOOk', args=[str(self.id)])

    def display_genre(self):
        """
        Creates a string for the Genre. This is required to display genre in Admin.
        Создает строку для жанра. Это необходимо для отображения жанра в Admin.
        """
        return ', '.join([ genre.name for genre in self.genre.all()[:3] ])
    display_genre.short_description = 'Жанрррр'

class BookInstance(models.Model):
    """
    Модель копии книги (i.e. that can be borrowed from the library).
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                          help_text="Unique ID данной книги в этой библиотеке")  #help_text - это заголовок каждого поля админ интерфейса
    book = models.ForeignKey('Book', on_delete=models.SET_NULL, null=True, verbose_name='Произведение') #
    imprint = models.CharField(max_length=200)
    due_back = models.DateField(null=True, blank=True, verbose_name='До какого срока')
    # Заёмщик, должник
    borrower = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='У кого?') 
    """определение свойства внутри класса BookInstance"""
    @property
    def is_overdue(self):
        if self.due_back and date.today() > self.due_back:  # Сначала мы проверим, является ли due_back пустым, прежде 
            # чем проводить сравнение. Пустое поле due_back заставило Django выкидывать ошибку, а не показывать страницу: пустые 
            # значения не сопоставимы. Это не то, что мы хотели бы, чтобы наши пользователи испытывали!
            return True
        return False


    LOAN_STATUS = (
        ('р', 'на Реставрации'),
        ('в', 'Выдан'),
        ('д', 'Доступен'),
        ('з', 'Зарезервирован'),
    )

    status = models.CharField(max_length=1, choices=LOAN_STATUS, blank=True, default='m', help_text='Book availability') 
    #help_text - это заголовок каждого поля админ интерфейса

    class Meta:
        ordering = ["due_back"]

    def __str__(self):
        """
        String for representing the Model object
        """
        return '%s (%s)' % (self.id, self.book.title)

class Author(models.Model):
    """
    Модель Автора
    """
    first_name = models.CharField(max_length=21, help_text="Имя", verbose_name='ИМЯ')
    last_name = models.CharField(max_length=10, help_text="Фамилия", verbose_name="ФАМИЛИЯ")
    foto = models.ImageField(verbose_name='Портрет', upload_to = 'fts-athrs', null = True)
    date_of_birth = models.DateField(null=True, blank=True, verbose_name='Жил с...')
    date_of_death = models.DateField( null=True, blank=True, verbose_name='...жил до')

    def get_absolute_url(self):
        """
        Возвращает url-адрес для доступа к конкретному экземпляру author.
        """
        return reverse('authOr', args=[str(self.id)])

    def __str__(self):
        """
        Строка для представления объекта модели.
        """
        return '%s, %s' % (self.last_name, self.first_name)
