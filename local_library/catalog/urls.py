from django.urls import path
from . import views
#from django.conf.urls import url

urlpatterns = [
    path('', views.index, name='index'),                # в locallibraruy/urls.py  - указано, что от /catalog/ 
    path('books', views.books_view, name='bOOks'),
    path('authors', views.autors_view, name='authOrs'), # пройдя то этой ссылке, вызывается метод autors_view из файла view.py
    path('book/view/<pk>', views.book_view, name='bOOk'),
    path('mybooks', views.mybook_view, name='mybOOks'),
    path('author/view/<pk>', views.autor_view, name='authOr'),
    path('author/create', views.autor_new, name='authCrd'),  
#    path('book/create', views.BookCreate.as_view(), name='bOOkCrd')
]
# urlpatterns += [   
#   url(r'^mybooks/$', views.LoanedBooksByUserListView.as_view(), name='my-bOrrOwed'), # Маршрут для class LoanedBooksByUserListView 
#]


