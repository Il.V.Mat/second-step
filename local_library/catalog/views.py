from django.shortcuts import render, redirect
from django.core.paginator import Paginator
#from .models import Book, Author, BookInstance, Genre # Создаются отображение Моделей
#-*-заменяет перечисление Моделей, говоря "все"
from .models import * 
from django.views import generic    # обобщенный класс уже реализует большую часть; ОбщиеПредставления GenericView - абстрагируют общие 
# шаблоны до такой степени, что вам даже не нужно писать код Python для написания приложения.

#from django.http import Http404,HttpResponseRedirect,  # class HttpResponseRedirect - перенаправляет на другой адрес (HTTP код статуса 302). 




def index(request):
    """
    Функция отображения для домашней страницы сайта.
    """

    # Генерация "количеств" некоторых главных объектов
    num_books = Book.objects.all().count()
    num_instances = BookInstance.objects.all().count()

    # Доступные книги (статус = 'a')
    num_instances_available = BookInstance.objects.filter(status__exact='a').count()
    num_authors = Author.objects.count()    # Метод 'all()' применен по умолчанию.

                    # Количество посещений этого представления, подсчитанное в переменной сессии. 
                    # HttpRequest передается как первый аргумент в каждое отображение.
    num_visits=request.session.get('nm_vsts', 0)     #Получаем значение 'num_visits' из сессии, возвращая 0, если оно не было установлено ранее
    request.session['nm_vsts'] = num_visits+1        #Каждый раз при получении запроса, мы увеличиваем данное значение на единицу и сохраняем его обратно в сессии (до следующего посещения данной страницы пользователем)
    num_visits_finish = ''
    remain_visits = num_visits
    if remain_visits < 12 or remain_visits > 19:
        remain_visits = num_visits % 10
        print("remain_visits = ", remain_visits)
    if remain_visits >= 2 or remain_visits <= 4:
        num_visits_finish = 'разa'
        print("raza remain_visits = ", remain_visits)
    else: num_visits_finish = 'раз'
     # Отрисовка HTML-шаблона index.html с данными внутри переменной контекста context=
    return render(
        request,
        'index.html',
        context={'nm_bks': num_books, 
                'num_instances': num_instances, 'num_instances_available': num_instances_available, 
                'nm_athrs': num_authors,
               'nm_vsts': num_visits, 'nm_vsts_fnsh': num_visits_finish }, # количество посещений. Переменная nm_vsts передается в шаблон через переменную контекста context=
            )


def books_view(request):
    """
    Функция отображения для страницы сайта Книги.
    """
    book_list = Book.objects.all() #запрашиваем из БД все объекты Book, и сохраняем в переменную book_list
    page = request.GET.get('page',1)
    paginator = Paginator(book_list,5)
    page_books_list = paginator.get_page(page)
    # Отрисовка HTML-шаблона index.html с данными внутри переменной контекста context=
    return render(
            request,
            'ctlg/book_list.html', #указывается html страница откуда будут приходить запросы request
            context={'p_bk_lst': page_books_list} #создаются имена к спискам для передачи в html при запросе request с указанной html страницы
        )

def book_view(request,pk):
    """
    Функция отображения для страницы сайта Книга.
    """
    # Запрашиваем запись с соответствующим <pk> в БД Book
    book = Book.objects.get(pk=pk) 
    # Отрисовка HTML-шаблона index.html с данными внутри переменной контекста context=
    return render(
        request,
        'ctlg/book_detail.html',
        context={'bk_dtl': book},
    )

 
def autors_view(request):
    """
    Функция отображения для страницы сайта Авторы.
    """
    
    autors_list = Author.objects.all() # Вызов всех записей из модели БД Авторы
    page = request.GET.get('page',1)
    paginator = Paginator(autors_list,3)
    page_autors_list = paginator.get_page(page)
    # Отрисовка HTML-шаблона index.html с данными внутри переменной контекста context=
    return render(
            request,
            'ctlg/autors_list.html',
            context={'p_athrs_lst': page_autors_list}
        )



def autor_view(request,pk):
    """
    Функция отображения для страницы сайта Автор.
    """
    
    autor_detail = Author.objects.get(pk=pk) # Вызов Автора <pk>
    book_list_for_author = Book.objects.filter(author = autor_detail.id) #фильтрация Book из БД по имени Author
    # Отрисовка HTML-шаблона index.html с данными внутри переменной контекста context=
    return render(  #отрисовка на экране
        request,
        'ctlg/autor_detail.html',
        context={'athr_dtl': autor_detail,'bk_lst_fr_athr':book_list_for_author}, #создаются имена к спискам с данными для передачи в html
    )



def mybook_view(request):
    """
    Функция отображения для страницы сайта Взятые книги.
    """
    if request.user.is_authenticated != True:   #для зарегистрированного пользователя
            return HttpResponseRedirect('/')    #если польз. не зарегистрирован, то послать его /
            # from django.http import Http404,HttpResponseRedirect вверху этого файла.

    list_my_books = BookInstance.objects.filter(borrower=request.user) # сформировать список list_my_books из 
    # объектов модели BookInstance, но отфильтровав filter(borrower по запрашиваемому пользователю request.user
    page = request.GET.get('page',1)
    paginator = Paginator(list_my_books,1)
    page_list_my_books = paginator.get_page(page)

    return render(  #отрисовать на экране
        request,
        'ctlg/bookinstance_list_borrowed_user.html',        #отрисовать в указанном шаблоне html
        context={'mybks_lst': page_list_my_books}, # Cписок с данными для передачи в html назвать mybks_lst

    )

from django.core.files.storage import FileSystemStorage

def autor_new(request): # метод обработки для соответствующего url маршрут 
    """
    Функция создания новой записи об Авторе и
    обработка внесённых данных на странице ввода.
    """
    if request.method == 'POST':    # проверка метода и, если передача данных, то отправляются в БД
         fs = FileSystemStorage(location='/mediaa/fts-athrs/')
         avtor = Author.objects.create()
         avtor.first_name = request.POST.get('first_name', None)
         avtor.last_name = request.POST.get('last_name', None)
         avtor.date_of_birth = request.POST.get('date_of_birth', None)
         avtor.date_of_death = request.POST.get('date_of_death', None)
       
         avtor.foto = request.FILES['photo'] 
         avtor.save() 
         
    return render(  #отрисовка на экране. Возвращает шаблон
        request,        #по запросу
        'ctlg/autor_form.html'     # указываем словарь, который нужно выводить
       )

                